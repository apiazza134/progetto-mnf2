#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import pprint

from scipy.optimize import curve_fit


L, J, h, λ, E0, E1, M, Mabs, M01 = np.loadtxt(sys.argv[1], unpack=True)
L = L.astype(int)

M = Mabs

Ls = []
hs = []
magns = []
chis = []

for i in range(min(L), max(L)+1):
    mask = (L == i)
    Ls.append(i)
    hs.append(h[mask])
    magns.append(M[mask])

for i in range(len(Ls)):
    arg = hs[i].argsort()
    hs[i] = hs[i][arg]
    magns[i] = magns[i][arg]


def derivata(f, x):
    return (f[1:] - f[:-1])/(x[1:] - x[:-1])


for i in range(len(Ls)):
    chis.append(derivata(magns[i], hs[i]))

nu = 1
beta = 1/8
# gamma = 0.96830694 # 7/4
hc = 1


def chimax(L, gamma, cusu):
    return L**(gamma/nu) * cusu

_x = np.array(Ls)
_y = np.array([chis[i].min() for i in range(len(Ls))])

opt, cov = curve_fit(chimax, _x, _y, p0=(7/4, -11))
print(opt)

xx = np.linspace(_x.min(), _x.max(), 50)

plt.plot(_x, _y, marker='x')
plt.plot(xx, chimax(xx, *opt))
plt.show()
plt.close()

gamma = opt[0]

tildeM = []
tildeChi = []
x = []

for i in range(len(Ls)):
    x.append(Ls[i]**nu * (hs[i]/hc - 1))
    tildeM.append(magns[i] * Ls[i]**(beta/nu))
    tildeChi.append(chis[i] * Ls[i]**(-gamma/nu))

leg = []
for i in range(len(Ls)):
    # plt.plot(hs[i], magns[i], linestyle='', marker='x')
    _x = hs[i][1:]
    _y = chis[i]

    arg = _x.argsort()

    # plt.plot(_x[arg], _y[arg], marker='x')
    plt.plot(x[i][arg], tildeChi[i][arg], marker='x')


    # plt.plot(x[i][arg], tildeM[i][arg], marker='x')
    leg.append(f'L = {Ls[i]}')

plt.legend(leg)

plt.show()
