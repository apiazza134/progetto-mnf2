#!/bin/bash

# parametri fisici
L=$(seq 4 1 16)
J=1
lambda=0 #$(seq 0.001 1 0.001)
Hs=$(seq 0.85 0.001 1.15)
neigs=2
boundary="PBC"

# parametri non fisici
exeName=qising
remotePath=/root/qising
sshdelay=0.1
jobsLocal=15
outdir=data
outfile=$outdir/$(date +%Y-%m-%d_%H-%M-%S).dat
sshloginfile=hosts

_hosts=$(cat $sshloginfile | grep -v '#' | cut -d '/' -f 2)

function echoc {
    echo -e "\e[0;32m=== $1 ===\e[0m"
}

case $1 in
    compile)
        echoc "Compilo"
        mkdir -p build && cd build
        cmake ..
        make -j 4
        ;;

    clean)
        rm -rf build/*
        ;;

    run)
        echoc "Lancio il conto"
        mkdir -p $outdir
        parallel -j $jobsLocal \
                 build/$exeName {1} $J {2} {3} $neigs $boundary ::: $L ::: $Hs ::: $lambda | tee $outfile
        ;;

    connect)
        echoc "Apro i socket"
        parallel ssh -fN ::: $_hosts
        ;;

    disconnect)
        echoc "Killo i socket"
        pkill -f "ssh -fN"
        ;;

    prepare-remote)
        echoc "Installo pacchetti necessari"
        parallel --sshloginfile $sshloginfile --nonall apt-get update -yq '&&' \
                 apt-get install -yq parallel libeigen3-dev cmake '&&' \
                 mv /usr/include/eigen3/Eigen /usr/include/Eigen '&&' \
                 mv /usr/include/eigen3/unsupported /usr/include/unsupported '&&' \
                 rm -r /usr/include/eigen3
        parallel rsync -r /usr/include/Spectra {}:/usr/include/ ::: $_hosts
        ;;

    copy-remote)
        echoc "Copio i sorgenti"
        parallel rsync --exclude=build --exclude=CMakeFiles \
                 --exclude=CMakeCache.txt --exclude=$exeName --exclude="*.dat" \
                 -aR ./ {}:$remotePath/ ::: $_hosts
        ;;

    compile-remote)
        echoc "Compilo in remoto"
        parallel --sshloginfile $sshloginfile --workdir $remotePath --nonall cmake . '&&' make -j100
        ;;

    clean-remote)
        echoc "Cancello le cartelle remote"
        parallel --sshloginfile $sshloginfile --nonall rm -r $remotePath
        ;;

    run-remote)
        echoc "Lancio il conto in remoto"
        mkdir -p $outdir
        parallel --ssh "ssh -q" --sshloginfile $sshloginfile --sshdelay $sshdelay \
                 $remotePath/$exeName {1} $J {2} {3} $neigs $boundary ::: $L ::: $Hs ::: $lambda | tee $outfile
        ;;

    kill-remote)
        echoc "Killo i conti lanciati in remoto"
        parallel --sshloginfile $sshloginfile --nonall pkill $exeName
        ;;

    copy-compile-run)
        $0 clean-remote
        $0 copy-remote
        $0 compile-remote
        $0 run-remote
        ;;

    *)
        cat <<EOF
Parallelization helper, by Marco Venuti.
SSH control-master must be set up.

Usage:
Local commands
* compile
* clean
* run

Remote commands
* connect
* disconnect
* prepare-remote
* copy-remote
* compile-remote
* clean-remote
* run-remote
* kill-remote
* copy-compile-run
EOF
        ;;
esac
