#include <iostream>
#include <fstream>
// #include <iomanip>
#include <string>
#include <time.h>

#include <algorithm>
#include <iterator>
#include <fstream>

#include <Eigen/Sparse>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/MatOp/SparseSymMatProd.h>

using namespace std;

enum spin {up = 0, down = 1};
enum boundary {OBC, PBC};

typedef Eigen::SparseMatrix<double> SpMat;

inline int bit(const unsigned number, const unsigned position)
{
    return (number >> position) & 1;
}

inline unsigned log2(unsigned arg)
{
    unsigned count = 0;
    while (arg >>= 1) ++count;
    return count;
}

void PrintSpin(const spin s)
{
    cout << ((s == up) ? "↑" : "↓");
}

void PrintSpinString(const unsigned n, const unsigned L)
{
    cout << "|";
    for (unsigned b = 0; b < L; b++) {
        PrintSpin(spin(bit(n, L - 1 - b)));
    }
    cout << "⟩";
}

const SpMat Hamiltonian(const boundary BC, const unsigned L, const double J, const double h, const double lambda)
{
    const unsigned dim = pow(2, L);

    SpMat H(dim, dim);
    H.reserve(L * dim);

    for (unsigned i = 0; i < dim; i++) {
        int sumi = 0, sump = 0;

        // Interaction term
        unsigned XOR = (i ^ (i >> 1));
        for (unsigned spin_elem = 0; spin_elem < L - 1; spin_elem++) {
            sumi += bit(XOR, spin_elem);
        }
        if (BC == PBC)
            sumi += bit(i, 0) ^ bit(i, L-1);

        // Parallel and transverse field
        for (unsigned spin_elem = 0; spin_elem < L; spin_elem++) {
            sump += bit(i, spin_elem);
            H.insert((i ^ (1 << spin_elem)), i) = -h;
        }

        int L_inter = (BC == PBC) ? L : L-1;
        H.insert(i, i) = - J * (L_inter - 2*sumi) - lambda * ((int)L - 2*sump);
    }

    H.makeCompressed(); // vedere di vedere

    return H;
}

double MagnetizationZ(const Eigen::VectorXd psi)
{
    const unsigned dim = psi.size();
    const unsigned L = log2(dim);

    double MZ = 0;
    int sum;

    for (unsigned i = 0; i < dim; i++) {
        sum = 0;

        for (unsigned spin_elem = 0; spin_elem < L; ++spin_elem)
            sum += bit(i, spin_elem);

        MZ += psi(i) * psi(i) * ((int)L - 2*sum); // questa dovrebbe venire sempre esattamente zero secondo Rossini
    }

    return MZ/L;
}

double MagnetizationAbsZ(const Eigen::VectorXd psi)
{
    const unsigned dim = psi.size();
    const unsigned L = log2(dim);

    double MZ = 0;
    int sum;

    for (unsigned i = 0; i < dim; i++) {
        sum = 0;

        for (unsigned spin_elem = 0; spin_elem < L; spin_elem++)
            sum += bit(i, spin_elem);

        MZ += psi(i) * psi(i) * abs((int)L - 2*sum);
    }

    return MZ/L;
}

double Magnetization01Z(const Eigen::VectorXd psi0, const Eigen::VectorXd psi1)
{
    Eigen::VectorXd up = (psi0 + psi1)/sqrt(2);
    return MagnetizationZ(up);
}

vector<unsigned> generateIplus(const unsigned L)
/* Genera il vettore di indici corrispondenti all'insieme I^+ */
{
    unsigned dim = pow(2,L);
    unsigned count0, count1;
    vector<unsigned> Iplus;
    Iplus.reserve(ceil(L/2.));
    for (unsigned n = 0; n < dim; ++n) {
        count0 = 0;
        count1 = 0;
        for (unsigned b = 0; b < L; ++b) {
            // Conta il numero di spin up
            (bit(n, b) == 0) ? (count0++) : (count1++);
        }
        // cout << n << ": " << count0 << "|" << count1 << "\n";
        if ((count1 > count0) || ((count0 == count1) && (n & 1))) {
            // Più di metà spin sono up oppure esattamente metà sono up e il LSB è 1
            Iplus.push_back(n);
        }
    }
    return Iplus;
}


double vZw(Eigen::VectorXd v, Eigen::VectorXd w)
{
    double result = 0;
    int dim = v.size();
    for (int i = 0; i < dim; i++){
        result += v(i) * w(dim - 1 - i);
    }
    return result;
}

double SnMinusAn(Eigen::VectorXd v, Eigen::VectorXd w)
{
    const unsigned dim = v.size();
    const unsigned L = log2(dim);
    vector<unsigned> Iplus;

    double result = 0;

    Iplus = generateIplus(L);

    for (unsigned n: Iplus){
        result += pow(v(n) - w(n), 2);
    }

    return result;
}

int main(int argc, char *argv[])
{
    // Input parametri di simulazione
    if (argc != 7) {
        cout << "Not all parameters were given: exiting.\n";
        return 1;
    }

    const unsigned L      = stoi(argv[1]);
    const double   J      = stod(argv[2]);
    const double   h      = stod(argv[3]);
    const double   lambda = stod(argv[4]);
    const unsigned nevalues = stoi(argv[5]);
    const boundary BC     = strcmp(argv[6], "PBC") ? OBC : PBC;

    // DEBUG
    // cout << L << " " << J << " " << h << " " << lambda << " " << nevalues << " " << BC << "\n";

    // Generazione Hamiltoniana
    SpMat H = Hamiltonian(BC, L, J, h, lambda);

    // Diagonalizzazione
    Spectra::SparseSymMatProd<double> op(H);
    Spectra::SymEigsSolver
        <double, Spectra::SMALLEST_ALGE, Spectra::SparseSymMatProd<double>>
        eigs(&op, nevalues, 8*nevalues); // TODO: cusmanare la dimensione dello spazio di Krilov

    srand(time(NULL));
    eigs.init();
    eigs.compute();

    // Processing in caso la diagonalizzazione abbia successo
    if (eigs.info() == Spectra::SUCCESSFUL) {
        auto evalues = eigs.eigenvalues();
        auto evectors = eigs.eigenvectors();

        cout << L << " "
             << J << " "
             << h << " "
             << lambda << " ";

        for (unsigned i = 0; i < nevalues; i++)
            cout << evalues(nevalues - 1 - i) << " ";

        cout << MagnetizationZ(evectors.col(nevalues - 1)) << " "
             << MagnetizationAbsZ(evectors.col(nevalues - 1)) << " "
             << Magnetization01Z(evectors.col(nevalues - 1), evectors.col(nevalues - 2)) << " ";

        cout << vZw(evectors.col(nevalues - 1), evectors.col(nevalues - 1)) << " "
             << vZw(evectors.col(nevalues - 2), evectors.col(nevalues - 2)) << " ";

        cout << SnMinusAn(evectors.col(nevalues - 2), evectors.col(nevalues - 1)) << " ";

        cout << "\n";

        unsigned dim = pow(2,L);
        vector<unsigned> Iplus;
        auto v0 = evectors.col(nevalues - 1);
        auto v1 = evectors.col(nevalues - 2);
        Iplus = generateIplus(L);
        std::ofstream myfile;
        myfile.open("out.dat");
        for (auto n: Iplus){
            myfile << n << " " << v1[n] - v0[n] << "\n";
        }
        myfile.close();

    } else {
        cout << eigs.info() << "\n";
        return 1;
    }

    return 0;
}
