#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt

style = {
    # 'text.usetex': True,
    # 'text.latex.preamble': r'\usepackage[euler-digits]{eulervm}',
    'font.family': 'serif',
    'font.size': 15,
    'axes.titlesize': 17,
    'axes.labelsize': 17,
    'lines.linewidth': 0.5,
    'savefig.dpi': 600,
    'savefig.bbox': 'tight'
}
plt.style.use(style)

L, J, h, lbd, E1, E0, Mz = np.loadtxt(sys.argv[1], unpack=True)

plt.plot(h, E1 - E0, linestyle='', marker='.', markersize=20, color='blue')
plt.show()

plt.plot(h, Mz, linestyle='', marker='.', markersize=20, color='blue')
plt.show()
